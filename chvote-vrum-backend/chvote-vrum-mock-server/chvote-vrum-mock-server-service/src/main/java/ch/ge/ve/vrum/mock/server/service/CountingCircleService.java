/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.service;

import ch.ge.ve.vrum.repository.votingcard.CountingCircleRepository;
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle;
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service that creates and persists mocked {@link CountingCircle} entities.
 */
@Service
public class CountingCircleService {
  private static final Map<String, String> COUNTING_CIRCLES = ImmutableMap.of(
      "6621", "Genève",
      "6612", "Chêne-Bougeries",
      "6608", "Carouge",
      "6628", "Lancy",
      "6624", "Gy"
  );

  private final CountingCircleRepository countingCircleRepository;

  /**
   * Create a new counting circle service.
   *
   * @param countingCircleRepository the {@link CountingCircleRepository}.
   */
  @Autowired
  public CountingCircleService(CountingCircleRepository countingCircleRepository) {
    this.countingCircleRepository = countingCircleRepository;
  }

  /**
   * Create and persist a set of predefined {@link DomainOfInfluence} entities:
   *
   * <h3 id="predefined">Predefined Counting Circles</h3>
   * <table class="striped" style="text-align:left">
   * <thead>
   * <tr>
   * <th scope="col">businessId</th>
   * <th scope="col">name</th>
   * </tr>
   * </thead>
   * <tbody>
   * <tr>
   * <th scope="row">6621</th>
   * <td>Genève</td>
   * </tr>
   * <tr>
   * <th scope="row">6612</th>
   * <td>Chêne-Bougeries</td>
   * </tr>
   * <tr>
   * <th scope="row">6608</th>
   * <td>Carouge</td>
   * </tr>
   * <tr>
   * <th scope="row">6628</th>
   * <td>Lancy</td>
   * </tr>
   * <tr>
   * <th scope="row">6624</th>
   * <td>Gy</td>
   * </tr>
   * </tbody>
   * </table>
   */
  @Transactional
  public void createAll() {
    COUNTING_CIRCLES.forEach((businessId, name) -> {
      CountingCircle countingCircle = new CountingCircle();
      countingCircle.setBusinessId(businessId);
      countingCircle.setName(name);
      countingCircleRepository.save(countingCircle);
    });
  }

  /**
   * Delete all the persisted entities regardless they were created by this service.
   */
  @Transactional
  public void deleteAll() {
    countingCircleRepository.deleteAll();
  }
}
