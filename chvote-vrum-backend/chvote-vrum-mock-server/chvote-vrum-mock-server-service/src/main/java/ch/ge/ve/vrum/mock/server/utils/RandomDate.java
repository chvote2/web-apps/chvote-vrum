/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A random date generator isolated to the current thread.
 */
public final class RandomDate {

  /**
   * Returns a pseudorandom {@link LocalDate} value between the given origin (inclusive) and bound (exclusive).
   *
   * @param origin the origin (inclusive).
   * @param bound  the bound (exclusive).
   *
   * @return the pseudorandom {@link LocalDate} value.
   */
  public static LocalDate get(LocalDate origin, LocalDate bound) {
    return get(origin.atStartOfDay(), bound.atStartOfDay()).toLocalDate();
  }

  /**
   * Returns a pseudorandom {@link LocalDateTime} value between the given origin (inclusive) and bound (exclusive).
   *
   * @param origin the origin (inclusive).
   * @param bound  the bound (exclusive).
   *
   * @return the pseudorandom {@link LocalDateTime} value.
   */
  public static LocalDateTime get(LocalDateTime origin, LocalDateTime bound) {
    ZoneId zoneId = TimeZone.getDefault().toZoneId();
    Instant randomInstant = get(origin.atZone(zoneId).toInstant(), bound.atZone(zoneId).toInstant());

    return LocalDateTime.ofInstant(randomInstant, zoneId);
  }

  /**
   * Returns a pseudorandom {@link Instant} value between the given origin (inclusive) and bound (exclusive).
   *
   * @param origin the origin (inclusive).
   * @param bound  the bound (exclusive).
   *
   * @return the pseudorandom {@link Instant} value.
   */
  public static Instant get(Instant origin, Instant bound) {
    return Instant.ofEpochMilli(
        ThreadLocalRandom.current()
                         .longs(origin.toEpochMilli(), bound.toEpochMilli())
                         .findAny()
                         .getAsLong()
    );
  }

  /**
   * Hide utility class constructor.
   */
  private RandomDate() {

  }
}
