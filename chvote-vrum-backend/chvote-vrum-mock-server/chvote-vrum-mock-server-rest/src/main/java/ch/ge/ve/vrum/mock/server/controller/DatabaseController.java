/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.mock.server.controller;

import ch.ge.ve.vrum.mock.server.service.CountingCircleService;
import ch.ge.ve.vrum.mock.server.service.DomainOfInfluenceService;
import ch.ge.ve.vrum.mock.server.service.OperationService;
import ch.ge.ve.vrum.mock.server.service.VotingCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A REST controller that enables the manipulation of mocked entities at the database level.
 */
@RestController
@RequestMapping("/database")
@Api(
    value = "Database",
    tags = "Database")
public class DatabaseController {

  private static final Logger logger = LoggerFactory.getLogger(DatabaseController.class);

  private final DomainOfInfluenceService domainOfInfluenceService;
  private final CountingCircleService    countingCircleService;
  private final OperationService         operationService;
  private final VotingCardService        votingCardService;

  /**
   * Create a new database controller.
   *
   * @param domainOfInfluenceService the {@link DomainOfInfluenceService}.
   * @param countingCircleService    the {@link CountingCircleService}
   * @param operationService         the {@link OperationService}.
   * @param votingCardService        the {@link VotingCardService}.
   */
  @Autowired
  public DatabaseController(DomainOfInfluenceService domainOfInfluenceService,
                            CountingCircleService countingCircleService,
                            OperationService operationService,
                            VotingCardService votingCardService) {
    this.domainOfInfluenceService = domainOfInfluenceService;
    this.countingCircleService = countingCircleService;
    this.operationService = operationService;
    this.votingCardService = votingCardService;
  }


  /**
   * Clean the database.
   */
  @DeleteMapping
  @ApiOperation(
      value = "Empty the database",
      notes = "Delete all the entities persisted in the database.")
  public void reset() {
    logger.info("Start cleaning database ...");

    votingCardService.deleteAll();
    countingCircleService.deleteAll();
    domainOfInfluenceService.deleteAll();
    operationService.deleteAll();

    logger.info("The database has been cleaned !");
  }

  /**
   * Initialize the database with a set of predefined entities.
   *
   * @see CountingCircleService#createAll()
   * @see DomainOfInfluenceService#createAll()
   * @see OperationService#createAll()
   */
  @PostMapping
  @ApiOperation(
      value = "Init the database",
      notes = "Initialize the database with a set of predefined entities.\n\n" +
              "###Predefined counting circles\n\n" +
              "| Name            | Business Id |\n" +
              "| --------------- | ----------- |\n" +
              "| Genève          | 6621        |\n" +
              "| Chêne-Bougeries | 6612        |\n" +
              "| Carouge         | 6608        |\n" +
              "| Lancy           | 6628        |\n" +
              "| Gy              | 6624        |\n" +
              "###Predefined domains of influence\n\n" +
              "| Name            | Business Id |\n" +
              "| --------------- | ----------- |\n" +
              "| Genève          | GE          |\n" +
              "###Predefined operations\n\n" +
              "| Short label     | Protocol Id | Test |\n" +
              "| --------------- | ----------- | ---- |\n" +
              "| 20200922VP      | OP_1        | no   |\n" +
              "| 20211215VP      | OP_2        | no   |\n" +
              "| 20190306VP      | OP_3        | no   |\n" +
              "| 20221021VP      | OP_4        | no   |\n" +
              "| 21000101VP      | OP_5        | yes  |"
  )
  public void init() {
    logger.info("Start cleaning database ...");

    operationService.createAll();
    domainOfInfluenceService.createAll();
    countingCircleService.createAll();

    logger.info("The database has been cleaned !");
  }
}
