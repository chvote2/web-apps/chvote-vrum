/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.repository.votingcard;

import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The {@link VotingCard} repository.
 */
@Repository
public interface VotingCardRepository extends JpaRepository<VotingCard, Long> {

  /**
   * Find the single matching voting card matching the specified voter index and operation ID.
   *
   * @param operationId the operation ID
   * @param voterIndex  the voter index
   *
   * @return the matching {@code VotingCard}.
   */
  Optional<VotingCard> findByOperation_IdAndVoterIndex(Long operationId, Long voterIndex);

  /**
   * Find all voting card matching the specified identification code hash and operation ID.
   *
   * @param operationId            the {@link ch.ge.ve.vrum.repository.operation.entity.Operation#id}.
   * @param identificationCodeHash the {@link VotingCard#identificationCodeHash}
   *
   * @return a stream with all the matching {@link VotingCard} entities.
   */
  Stream<VotingCard> findByOperation_IdAndIdentificationCodeHash(Long operationId, String identificationCodeHash);

  /**
   * Find all voting card matching the specified local person ID and operation ID.
   *
   * @param operationId   the {@link ch.ge.ve.vrum.repository.operation.entity.Operation#id}.
   * @param localPersonId the {@link VotingCard#localPersonId}
   *
   * @return a stream with all the matching {@link VotingCard} entities.
   */
  Stream<VotingCard> findByOperation_IdAndLocalPersonId(Long operationId, String localPersonId);

}
