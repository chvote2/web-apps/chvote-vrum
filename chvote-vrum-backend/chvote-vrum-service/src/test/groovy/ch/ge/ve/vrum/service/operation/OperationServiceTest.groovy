/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.operation

import ch.ge.ve.vrum.repository.operation.OperationRepository
import ch.ge.ve.vrum.repository.operation.entity.Canton
import ch.ge.ve.vrum.repository.operation.entity.Operation
import java.time.LocalDateTime
import spock.lang.Specification

class OperationServiceTest extends Specification {

  def final static expectedOperations = [
          createOperation(1, false),
          createOperation(2, false),
          createOperation(3, false),
          createOperation(4, true)
  ]

  private OperationService service
  private OperationRepository operationRepository

  def setup() {
    operationRepository = Mock()
    service = new OperationService(operationRepository)
  }

  def "findAll should return all operations"() {
    given:
    operationRepository.findAll() >> expectedOperations

    when:
    def operations = service.findAll()

    then:
    operations.size() == expectedOperations.size()

    and:
    for (def i = 0; i < expectedOperations.size(); i++) {
      operations[i].id == expectedOperations[i].id
      operations[i].operationDate == expectedOperations[i].operationDate
      operations[i].shortLabel == expectedOperations[i].shortLabel
      operations[i].longLabel == expectedOperations[i].longLabel
      operations[i].test == expectedOperations[i].test
    }
  }

  def "findById should return only one operation"() {
    given:
    operationRepository.findById(0) >> Optional.of(expectedOperations[0])

    when:
    def operation = service.findById(0)

    then:
    operation.id == expectedOperations[0].id
    operation.operationDate == expectedOperations[0].operationDate
    operation.shortLabel == expectedOperations[0].shortLabel
    operation.longLabel == expectedOperations[0].longLabel
    operation.test == expectedOperations[0].test
  }

  def "findByTest should return exclusively test operations OR real operations"() {
    given:
    operationRepository.findByTest(test) >> expected.stream()

    when:
    def operations = service.findByTest(test)

    then:
    operations.size() == expected.size()

    and:
    for (def i = 0; i < operations.size(); i++) {
      operations[i].id == expected[i].id
      operations[i].operationDate == expected[i].operationDate
      operations[i].shortLabel == expected[i].shortLabel
      operations[i].longLabel == expected[i].longLabel
      operations[i].test == expected[i].test
    }

    where:
    test  | expected
    true  | [expectedOperations[3]]
    false | expectedOperations.take(3)
  }

  def static createOperation(Long id, boolean test) {
    def operation = new Operation()

    operation.id = id
    operation.operationDate = LocalDateTime.now()
    operation.protocolId = "PID_" + id
    operation.shortLabel = "Operation " + id
    operation.longLabel = "Long Label Operation " + id
    operation.canton = Canton.GE
    operation.test = test

    return operation
  }

}
