/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.notification

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient
import ch.ge.ve.chvote.pact.b2b.client.model.Canton
import ch.ge.ve.chvote.pact.b2b.client.model.Lang
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType
import ch.ge.ve.chvote.pact.b2b.client.model.Voter
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod
import ch.ge.ve.vrum.repository.operation.OperationRepository
import ch.ge.ve.vrum.repository.operation.entity.Operation
import ch.ge.ve.vrum.repository.votingcard.CountingCircleRepository
import ch.ge.ve.vrum.repository.votingcard.DomainOfInfluenceRepository
import ch.ge.ve.vrum.repository.votingcard.VotingCardRepository
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus
import java.time.LocalDateTime
import spock.lang.Specification

class OperationInitializerTest extends Specification {
  private PactB2BClient pactB2BClient
  private OperationRepository operationRepository
  private VotingCardRepository votingCardRepository
  private DomainOfInfluenceRepository domainOfInfluenceRepository
  private CountingCircleRepository countingCircleRepository
  private OperationInitializer operationInitializer

  def setup() {
    pactB2BClient = Mock(PactB2BClient)
    operationRepository = Mock(OperationRepository)
    votingCardRepository = Mock(VotingCardRepository)
    domainOfInfluenceRepository = Mock(DomainOfInfluenceRepository)
    countingCircleRepository = Mock(CountingCircleRepository)
    operationInitializer = new OperationInitializer(
            pactB2BClient, operationRepository, votingCardRepository,
            domainOfInfluenceRepository, countingCircleRepository
    )
  }

  def "should initialize an operation by requesting it to the pactB2B client"() {
    given:
    def operationDate = LocalDateTime.now().plusDays(1)
    def votingPeriod = new VotingPeriod(operationDate.plusDays(1), operationDate.plusDays(2), 50)
    pactB2BClient.getOperationConfiguration("1") >>
            new OperationBaseConfiguration(
                    "shortLabel", "longLabel", Canton.GE,
                    "votingCardLabel", "simulationName", Lang.FR, OperationType.TEST,
                    votingPeriod, operationDate, [], [])
    pactB2BClient.getVoters("1") >> []

    when:
    operationInitializer.initializeOperation("1")

    then:
    1 * operationRepository.save(*_) >> {
      args ->
        def result = args[0] as Operation
        result.shortLabel == "shortLabel"
        result.longLabel == "longLabel"
        result.protocolId == "1"
        result.canton == ch.ge.ve.vrum.repository.operation.entity.Canton.GE
        result.test
        result.operationDate == operationDate
        return result
    }
    0 * votingCardRepository.save(*_)
    0 * domainOfInfluenceRepository.save(*_)
    0 * countingCircleRepository.save(*_)
  }

  def createDomainOfInfluences() {

  }

  def "should initialize the voting cards by requesting them to the pactB2B client"() {
    given:
    def operationDate = LocalDateTime.now().plusDays(1)
    def votingPeriod = new VotingPeriod(operationDate.plusDays(1), operationDate.plusDays(2), 50)
    pactB2BClient.getOperationConfiguration("1") >>
            new OperationBaseConfiguration(
                    "shortLabel", "longLabel", Canton.GE,
                    "votingCardLabel", "simulationName", Lang.FR, OperationType.TEST,
                    votingPeriod, operationDate, [], [])
    operationRepository.save(*_) >> {
      args ->
        def result = args[0] as Operation
        result.id = 0
        return result
    }
    domainOfInfluenceRepository.findByBusinessId(*_) >> Optional.empty()
    countingCircleRepository.findByBusinessId(*_) >> Optional.empty()
    pactB2BClient.getVoters("1") >> [
            new Voter(
                    [new ch.ge.ve.protocol.model.DomainOfInfluence("GE")],
                    "localPersonId",
                    15,
                    1998,
                    10,
                    3,
                    "identificsationCodeHash",
                    new ch.ge.ve.protocol.model.CountingCircle(0, "6621", "Genève")
            )
    ]

    when:
    operationInitializer.initializeOperation("1")

    then:
    1 * domainOfInfluenceRepository.save(*_) >> {
      args ->
        def result = args[0] as DomainOfInfluence
        result.id = 2
        result.name == "GE"
        result.businessId == "GE"
        return result
    }
    1 * countingCircleRepository.save(*_) >> {
      args ->
        def result = args[0] as CountingCircle
        result.id = 4
        result.name == "Genève"
        result.businessId == "6621"
        return result
    }
    1 * votingCardRepository.save(*_) >> {
      args ->
        def result = args[0] as VotingCard
        result.voterIndex == 15
        result.identificationCodeHash == "identificationHashCode"
        result.localPersonId == "localPersonId"
        result.voterBirthYear == 1998
        result.voterBirthMonth == 10
        result.voterBirthDay == 3

        result.operation.id == 0
        result.countingCircle.id == 4
        result.domainOfInfluences.size() == 1
        result.domainOfInfluences[0].id == 2

        result.votingRightStatus == VotingRightStatus.AVAILABLE

        // Should not be set at initialization
        result.votingDatetime == null
        result.votingChannel == null

        return result
    }
  }

  def "should initialize the voting cards even with already existing domain of influences and counting circles"() {
    given:
    def operationDate = LocalDateTime.now().plusDays(1)
    def votingPeriod = new VotingPeriod(operationDate.plusDays(1), operationDate.plusDays(2), 50)
    pactB2BClient.getOperationConfiguration("1") >>
            new OperationBaseConfiguration(
                    "shortLabel", "longLabel", Canton.GE,
                    "votingCardLabel", "simulationName", Lang.FR, OperationType.TEST,
                    votingPeriod, operationDate, [], [])
    operationRepository.save(*_) >> {
      args ->
        def result = args[0] as Operation
        result.id = 0
        return result
    }
    domainOfInfluenceRepository.findByBusinessId("GE") >> {
      def result = new DomainOfInfluence()
      result.id = 1
      result.name = "GE"
      result.businessId = "GE"
      return Optional.of(result)
    }
    countingCircleRepository.findByBusinessId("6621") >> {
      def result = new CountingCircle()
      result.id = 2
      result.name = "Genève"
      result.businessId = "6621"
      return Optional.of(result)
    }
    pactB2BClient.getVoters("1") >> [
            new Voter(
                    [new ch.ge.ve.protocol.model.DomainOfInfluence("GE")],
                    "localPersonId",
                    15,
                    1998,
                    10,
                    3,
                    "identificsationCodeHash",
                    new ch.ge.ve.protocol.model.CountingCircle(0, "6621", "Genève")
            )
    ]
    when:
    operationInitializer.initializeOperation("1")

    then:
    0 * domainOfInfluenceRepository.save(*_)
    0 * countingCircleRepository.save(*_)
    1 * votingCardRepository.save(*_) >> {
      args ->
        def result = args[0] as VotingCard
        result.voterIndex == 15
        result.identificationCodeHash == "identificationHashCode"
        result.localPersonId == "localPersonId"
        result.voterBirthYear == 1998
        result.voterBirthMonth == 10
        result.voterBirthDay == 3

        result.operation.id == 0
        result.countingCircle.id == 2
        result.domainOfInfluences.size() == 1
        result.domainOfInfluences[0].id == 1

        result.votingRightStatus == VotingRightStatus.AVAILABLE

        // Should not be set at initialization
        result.votingDatetime == null
        result.votingChannel == null

        return result
    }
  }
}
