/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.mapper

import ch.ge.ve.chvote.pact.b2b.client.model.Lang
import ch.ge.ve.chvote.pact.b2b.client.model.OperationBaseConfiguration
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod
import ch.ge.ve.vrum.repository.operation.entity.Canton
import ch.ge.ve.vrum.repository.operation.entity.Operation
import ch.ge.ve.vrum.repository.votingcard.entity.CountingCircle
import ch.ge.ve.vrum.repository.votingcard.entity.DomainOfInfluence
import ch.ge.ve.vrum.repository.votingcard.entity.VotingCard
import ch.ge.ve.vrum.repository.votingcard.entity.VotingChannel
import ch.ge.ve.vrum.repository.votingcard.entity.VotingRightStatus
import java.time.LocalDateTime
import spock.lang.Specification

class BeanMapperTest extends Specification {

  def "Mapping an Operation should return an OperationVo with the same values"() {
    given:
    def operation = createOperation(1)

    when:
    def operationVo = BeanMapper.map(operation)

    then:
    operationVo.id == operation.id
    operationVo.operationDate == operation.operationDate
    operationVo.shortLabel == operation.shortLabel
    operationVo.longLabel == operation.longLabel
    operationVo.test == operation.test
  }

  def "Mapping a VotingCard should return an VotingCardVo with the same values"() {
    given:
    def votingCard = createVotingCard(0)

    when:
    def votingCardVo = BeanMapper.map(votingCard)

    then:
    votingCardVo.id == votingCard.id
    votingCardVo.operationId == votingCard.operation.id
    votingCardVo.voterIndex == votingCard.voterIndex
    votingCardVo.identificationCodeHash == votingCard.identificationCodeHash
    votingCardVo.localPersonId == votingCard.localPersonId
    votingCardVo.voterBirthDate.voterBirthYear == votingCard.voterBirthYear
    votingCardVo.voterBirthDate.voterBirthMonth == votingCard.voterBirthMonth
    votingCardVo.voterBirthDate.voterBirthDay == votingCard.voterBirthDay
    votingCardVo.votingDatetime == votingCard.votingDatetime
    votingCardVo.votingRightStatus == votingCard.votingRightStatus
    votingCardVo.votingChannel == votingCard.votingChannel
    votingCardVo.countingCircle.businessId == votingCard.countingCircle.businessId
    votingCardVo.domainOfInfluences[0].businessId == votingCard.domainOfInfluences[0].businessId
    votingCardVo.domainOfInfluences[1].businessId == votingCard.domainOfInfluences[1].businessId
  }

  def "Mapping a CountingCircle should return a CountingCircleVo with the same values"() {
    given:
    def countingCircle = this.createCountingCircle()

    when:
    def countingCircleVo = BeanMapper.map(countingCircle)

    then:
    countingCircleVo.businessId == countingCircle.businessId
    countingCircleVo.name == countingCircle.name
  }

  def "Mapping a DomainOfInfluence should return a DomainOfInfluenceVo with the same values"() {
    given:
    def domainOfInfluence = createDomainOfInfluence("6621", "Genève")

    when:
    def domainOfInfluenceVo = BeanMapper.map(domainOfInfluence)

    then:
    domainOfInfluenceVo.businessId == domainOfInfluence.businessId
    domainOfInfluenceVo.name == domainOfInfluence.name
  }

  def "Mapping an OperationBaseConfiguration should return an Operation entity with the same values"() {
    given:
    def operationDate = LocalDateTime.now().plusDays(1)
    def votingPeriod = new VotingPeriod(operationDate.plusDays(1), operationDate.plusDays(2), 50)
    def configuration = new OperationBaseConfiguration(
            "shortLabel", "longLabel", ch.ge.ve.chvote.pact.b2b.client.model.Canton.VS,
            "votingCardLabel", "simulationName", Lang.FR, OperationType.REAL,
            votingPeriod, operationDate, [], [])

    when:
    def operation = BeanMapper.map(configuration, "1")

    then:
    operation.id == null
    operation.protocolId == "1"
    operation.shortLabel == "shortLabel"
    operation.longLabel == "longLabel"
    operation.canton == Canton.VS
    !operation.test
    operation.operationDate == operationDate
  }

  def createOperation(Long id) {
    def operation = new Operation()

    operation.id = id
    operation.operationDate = LocalDateTime.now()
    operation.protocolId = "PID_1"
    operation.shortLabel = "Operation 1"
    operation.longLabel = "Long Label Operation 1"
    operation.canton = Canton.GE

    return operation
  }

  def createVotingCard(Long id) {
    def votingCard = new VotingCard()

    votingCard.id = id
    votingCard.operation = createOperation(1)
    votingCard.voterIndex = 3
    votingCard.identificationCodeHash = "ID_HASH_1"
    votingCard.localPersonId = "L_PERSON_ID_1"
    votingCard.voterBirthYear = 1970
    votingCard.voterBirthMonth = 4
    votingCard.voterBirthDay = 5
    votingCard.votingDatetime = LocalDateTime.now()
    votingCard.votingRightStatus = VotingRightStatus.USED
    votingCard.votingChannel = VotingChannel.E_VOTING
    votingCard.countingCircle = createCountingCircle()
    votingCard.domainOfInfluences = [
            createDomainOfInfluence("6621", "Genève"),
            createDomainOfInfluence("6608", "Carouge")
    ]
    votingCard.domainOfInfluences[0].businessId = "GE"
    votingCard.domainOfInfluences[1].businessId = "GE"

    return votingCard
  }

  def createCountingCircle() {
    def countingCircle = new CountingCircle()

    countingCircle.businessId = "6621"
    countingCircle.name = "Genève"

    return countingCircle
  }

  def createDomainOfInfluence(String businessId, String name) {
    def domainOfInfluence = new DomainOfInfluence()

    domainOfInfluence.businessId = businessId
    domainOfInfluence.name = name

    return domainOfInfluence
  }
}
