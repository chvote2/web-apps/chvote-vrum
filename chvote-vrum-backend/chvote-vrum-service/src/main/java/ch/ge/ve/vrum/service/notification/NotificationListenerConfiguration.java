/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.service.notification;

import ch.ge.ve.chvote.pactback.notification.config.RabbitMqConfiguration;
import ch.ge.ve.chvote.pactback.notification.listener.NotificationEventsConsumersRegistry;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration class of the PACT notification registry.
 */
@Configuration
@ComponentScan("ch.ge.ve.chvote.pactback.notification")
@Import({RabbitMqConfiguration.class})
public class NotificationListenerConfiguration {
  private final NotificationEventsConsumersRegistry notificationEventsConsumersRegistry;
  private final OperationInitializer                operationInitializer;

  /**
   * Create a new notification listener configuration.
   *
   * @param notificationEventsConsumersRegistry the notification registry.
   * @param operationInitializer                the operation initializer that will be called once the voting material
   *                                            is ready.
   */
  @Autowired
  public NotificationListenerConfiguration(NotificationEventsConsumersRegistry notificationEventsConsumersRegistry,
                                           OperationInitializer operationInitializer) {
    this.notificationEventsConsumersRegistry = notificationEventsConsumersRegistry;
    this.operationInitializer = operationInitializer;
  }

  /**
   * Register all the notification consumers.
   */
  @PostConstruct
  public void registerConsumers() {
    notificationEventsConsumersRegistry.onVotingMaterialsGeneratedEvent(
        event -> operationInitializer.initializeOperation(event.getProtocolId())
    );
  }
}
