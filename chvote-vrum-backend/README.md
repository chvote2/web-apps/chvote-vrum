# CHVote: VRUM Backend

The voting rights usage manager (VRUM) server side application.

# Modules

- [chvote-vrum-repository](chvote-vrum-repository): A module that is responsible for the persistence of the VRUM application.
- [chvote-vrum-service](chvote-vrum-service):The set of business services of the VRUM backend application.
- [chvote-vrum-mock-server](chvote-vrum-mock-server): A server that allows the injection of mocked data directly into the VRUM repository for testing purposes.
- [chvote-vrum-rest](chvote-vrum-rest): The collection of REST interfaces of the VRUM backend application.

# Development guidelines

Refer to the common [CHVote java coding style guidelines](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-Java.md).

# Building

## Pre-requisites

* JDK 11
* Maven

## Build steps

To build the project simply do:

```sh
mvn clean verify
```

For every module a jacoco coverage report will be generated and stored under the `./target/site/jacoco/` subfolder of each module.

# Running

The application entry point is the `chvote-vrum-rest` module, all the other modules are contained here as dependencies.
To start the server that exposes the REST API simply do:

```sh
cd chvote-vrum-rest
mvn spring-boot:run -P development
```

**WARNING:** There is a recent bug in the `spring-boot-maven-plugin` that prevents the previous method from starting the 
REST API with the `development` profile activated, for the time being you can workaround this problem by activating the profile 
with the following environment variable:

```sh
export SPRING_PROFILES_ACTIVE=development
```

The REST API can then be queried at [http://localhost:8381/api/](http://localhost:8381/api/), e.g.:

```sh 
curl --user ge1:pass  http://localhost:8381/api/version
```

## Running the mock server

The mock server entry point is the `chvote-mock-server/chvote-mock-server-rest` submodule.
To run the mock server simply do:

```sh
cd chvote-mock-server/chvote-mock-server-rest
mvn spring-boot:run
```

**NOTE**: In order for the mock server to correctly start up, the VRUM application must already be running with the `developement` spring profile active.

An interactive Swagger API documentation page will be available at [http://localhost:48646/mock/swagger-ui.html](http://localhost:48646/mock/swagger-ui.html).
The mock server REST API can also be queried at [http://localhost:48646/mock/](http://localhost:48646/mock/), e.g:

```sh
# Reset the database
curl -X DELETE http://localhost:48646/mock/database

# Request the list of operations, should return an empty list
curl --user ge1:pass  http://localhost:8381/api/operations

# Intialize the database with a set of predefined operations
curl -X POST http://localhost:48646/mock/database

# Request the list of operations, should return a list with 5 mocked operations
curl --user ge1:pass  http://localhost:8381/api/operations
```

# Tips

## H2 database console

When activating spring profile `development`, you can connect to the H2 database console at http://localhost:8381/api/h2-console/ using the following parameters:

- *JDBC URL*: jdbc:h2:mem:test;MODE=ORACLE;DB_CLOSE_DELAY=-1
- *User Name*: h2_test
- *Password*: h2_test