/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller.version;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A DTO that represents the version of the application.
 */
public class VersionDto {
  private final String        buildNumber;
  private final LocalDateTime buildTimestamp;

  /**
   * Create a new version DTO.
   *
   * @param buildNumber    a string representing the current build number of the application.
   * @param buildTimestamp the build timestamp of the application in {@link DateTimeFormatter#ISO_DATE_TIME} format.
   */
  public VersionDto(String buildNumber, String buildTimestamp) {
    this.buildNumber = buildNumber.startsWith("@") ? "DEV" : buildNumber;

    if (buildTimestamp.startsWith("@")) {
      this.buildTimestamp = LocalDateTime.now();
    } else {
      LocalDateTime localDateTime;
      try {
        localDateTime = LocalDateTime.parse(buildTimestamp, DateTimeFormatter.ISO_DATE_TIME);
      } catch (DateTimeParseException e) {
        localDateTime = LocalDateTime.parse(buildTimestamp, DateTimeFormatter.ofPattern("yyyyMMdd-HHmm"));
      }
      this.buildTimestamp = localDateTime;
    }
  }

  public String getBuildNumber() {
    return buildNumber;
  }

  public LocalDateTime getBuildTimestamp() {
    return buildTimestamp;
  }
}