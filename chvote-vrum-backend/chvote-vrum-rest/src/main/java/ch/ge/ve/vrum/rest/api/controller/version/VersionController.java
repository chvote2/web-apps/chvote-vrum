/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller.version;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A specialized controller to get the application's current version information.
 */
@RestController
@RequestMapping("/version")
@Api(value = "Version", tags = "Version")
public class VersionController {
  private final VersionDto currentVersion;

  /**
   * Create a new version controller.
   *
   * @param buildNumber    a string representing the current build number of the application.
   * @param buildTimestamp the build timestamp of the application in {@link DateTimeFormatter#ISO_DATE_TIME} format.
   */
  @Autowired
  public VersionController(@Value("${env.buildNumber}") String buildNumber,
                           @Value("${env.buildTimestamp}") String buildTimestamp) {
    this.currentVersion = new VersionDto(buildNumber, buildTimestamp);
  }

  /**
   * Retrieve the current version of the application.
   *
   * @return the version of the application.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the version",
      notes = "Retrieve the version of the application." +
              "###Required user roles\n\n" +
              "* ROLE_USER")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource")
  })
  @PreAuthorize("hasRole('USER')")
  public VersionDto getCurrentVersion() {
    return currentVersion;
  }
}
