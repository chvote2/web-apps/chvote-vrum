/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.shared.config;

import java.sql.SQLException;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Specific configuration class of the VRUM backend application for
 * development, integration and UAT (rec)
 */
@Configuration
@Profile({"development","int","rec"})
public class CHVoteVRUMDevIntRecConfiguration {

  private static final Logger logger = LoggerFactory.getLogger(CHVoteVRUMDevIntRecConfiguration.class);

  /**
   * @return the created server
   *
   * @throws SQLException if we cannot connect
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  public Server h2Server() throws SQLException {
    String tcpPort = "48645";
    Server tcpServer = Server.createTcpServer("-web", "-tcp", "-tcpAllowOthers", "-tcpPort", tcpPort);
    logger.info("H2 database server exposed over TCP port {}", tcpPort);
    return tcpServer;
  }
}
