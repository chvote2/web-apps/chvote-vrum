/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.api.controller;

import ch.ge.ve.vrum.service.operation.OperationService;
import ch.ge.ve.vrum.service.operation.model.OperationDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * A {@link OperationDto} REST controller.
 */
@RestController
@RequestMapping("/operations")
@Api(value = "Operations", tags = "Operations")
public class OperationController {

  private final OperationService operationService;

  /**
   * Create a new operation controller.
   *
   * @param operationService the {@link OperationService}.
   */
  @Autowired
  public OperationController(OperationService operationService) {
    this.operationService = operationService;
  }

  /**
   * Retrieve all the registered operations.
   *
   * @return the list of all the operations.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve all the operations",
      notes = "Retrieve all the ongoing operations." +
              "###Required user roles\n\n" +
              "* ROLE_SELECT_OPERATION")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource")
  })
  @PreAuthorize("hasRole('SELECT_OPERATION')")
  public List<OperationDto> findAll() {
    return operationService.findAll();
  }

  /**
   * Retrieve a list of operations matching the specified test property.
   *
   * @param test whether the returned operations should be flagged as test operations.
   *
   * @return all matching operations.
   */
  @GetMapping(params = "test", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve the operations",
      notes = "Retrieve a list of operations matching the specified test property" +
              "###Required user roles\n\n" +
              "* ROLE_SELECT_OPERATION")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource")
  })
  @PreAuthorize("hasRole('SELECT_OPERATION')")
  public List<OperationDto> findByTest(
      @ApiParam(value = "The identification code") @RequestParam("test") boolean test
  ) {
    return operationService.findByTest(test);
  }

  /**
   * Retrieve an {@link OperationDto} by its id.
   *
   * @param operationId the operation id.
   *
   * @return the {@link OperationDto} matching the provided id.
   *
   * @throws ch.ge.ve.vrum.service.exception.EntityNotFoundException if there were no entities for the given
   *                                                                 parameters.
   */
  @GetMapping(value = "{operationId}",
              produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Retrieve an operation by id",
      notes = "Retrieve an ongoing operation by id." +
              "###Required user roles\n\n" +
              "* ROLE_SELECT_OPERATION")
  @ApiResponses(value = {
      @ApiResponse(code = 403, message = "The current user is not authorized to access this resource"),
      @ApiResponse(code = 404, message = "Operation not found")
  })
  @PreAuthorize("hasRole('SELECT_OPERATION')")
  public OperationDto findOne(
      @ApiParam(value = "The operation id") @PathVariable("operationId") Long operationId
  ) {
    return operationService.findById(operationId);
  }

}
