/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.shared.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Class representing a connected user.
 */
public class ConnectedUser {
  public static final String BELONGS_TO_REALM             = "BELONGS_TO_REALM:";
  public static final String BELONGS_TO_MANAGEMENT_ENTITY = "BELONGS_TO_MANAGEMENT_ENTITY:";

  private static final String                     TECHNICAL_USER       = "technical";
  private static final ThreadLocal<ConnectedUser> currentConnectedUser = new ThreadLocal<>();

  private final String      login;
  private final Set<String> roles;
  private final String      realm;
  private final String      managementEntity;

  /**
   * Set a connected user should only be used in WebSecurityConnectedInterceptor or tests
   */
  public static void set(String login, Collection<String> roles) {
    currentConnectedUser.set(new ConnectedUser(login, roles));
  }

  /**
   * @return The current connected user associated with request thread
   */
  public static ConnectedUser get() {
    return currentConnectedUser.get();
  }

  private ConnectedUser() {
    this.login = TECHNICAL_USER;
    this.realm = null;
    this.managementEntity = TECHNICAL_USER;
    this.roles = Collections.singleton("ROLE_TECHNICAL");
  }

  private ConnectedUser(String login, Collection<String> authorities) {
    this.login = login;
    realm = authorities.stream()
                       .filter(this::isCantonRole)
                       .findFirst()
                       .orElseThrow(() -> new IllegalStateException("Cannot find canton from user authorities"))
                       .substring(BELONGS_TO_REALM.length()).trim();

    managementEntity = authorities.stream()
                                  .filter(this::isManagementEntityRole)
                                  .findFirst()
                                  .orElseThrow(
                                      () -> new IllegalStateException("Cannot find canton from user authorities"))
                                  .substring(BELONGS_TO_MANAGEMENT_ENTITY.length()).trim();

    this.roles = new HashSet<>(authorities);
    this.roles.removeIf(role -> isCantonRole(role) || isManagementEntityRole(role));
  }

  private boolean isManagementEntityRole(String role) {
    return role.startsWith(BELONGS_TO_MANAGEMENT_ENTITY);
  }

  private boolean isCantonRole(String role) {
    return role.startsWith(BELONGS_TO_REALM);
  }

  public String getLogin() {
    return login;
  }

  public Set<String> getRoles() {
    return Collections.unmodifiableSet(roles);
  }

  public String getRealm() {
    return realm;
  }

  public String getManagementEntity() {
    return managementEntity;
  }

  /**
   * Cleanup connected user
   */
  public static void unset() {
    currentConnectedUser.remove();
  }

  public static void technical() {
    currentConnectedUser.set(new ConnectedUser());
  }

  public static void set(ConnectedUser connectedUser) {
    currentConnectedUser.set(connectedUser);
  }

}
