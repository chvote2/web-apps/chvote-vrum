/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.vrum.rest.shared.model


import static ch.ge.ve.vrum.rest.shared.model.ConnectedUser.BELONGS_TO_MANAGEMENT_ENTITY
import static ch.ge.ve.vrum.rest.shared.model.ConnectedUser.BELONGS_TO_REALM

import spock.lang.Specification

class ConnectedUserTest extends Specification {

  def static MANAGEMENT_ENTITY = "Canton de Genève"

  def "set(String, [String]) should create a new connected user"() {
    def loginStub = "test"
    def rolesStub = ["USER", BELONGS_TO_REALM + "GENEVE", BELONGS_TO_MANAGEMENT_ENTITY + MANAGEMENT_ENTITY]

    when:
    ConnectedUser.set(loginStub, rolesStub)

    then:
    ConnectedUser connectedUser = ConnectedUser.get()
    connectedUser.login == loginStub
    connectedUser.realm == rolesStub[1].substring(BELONGS_TO_REALM.length())
    connectedUser.managementEntity == rolesStub[2].substring(BELONGS_TO_MANAGEMENT_ENTITY.length())
    connectedUser.roles.size() == 1
  }

  def "set(String, [String]) should throw an exception when the user has no realm"() {
    def loginStub = "test"
    def rolesStub = ["USER", BELONGS_TO_MANAGEMENT_ENTITY + MANAGEMENT_ENTITY]

    when:
    ConnectedUser.set(loginStub, rolesStub)

    then:
    thrown(IllegalStateException)
  }

  def "technical should create a new technical user"() {
    when:
    ConnectedUser.technical()

    then:
    ConnectedUser connectedUser = ConnectedUser.get()
    connectedUser.login == "technical"
    connectedUser.realm == null
    connectedUser.managementEntity == "technical"
    connectedUser.roles.size() == 1
  }

  def "unset should remove the currently connected user"() {
    given:
    ConnectedUser.technical()

    when:
    ConnectedUser.unset()

    then:
    ConnectedUser.get() == null
  }

  def "set(ConnectedUser) should create a new connected user"() {
    given:
    ConnectedUser.technical()
    ConnectedUser user = ConnectedUser.get()
    ConnectedUser.unset()

    when:
    ConnectedUser.set(user)

    then:
    ConnectedUser.get() != null
  }

}
