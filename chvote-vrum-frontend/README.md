# CHVote: VRUM Frontend

The voting rights usage manager (VRUM) web application.

# Development guidelines

Refer to the common [CHVote typescript coding style guidelines](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-FrontEnd.md).

# Building

## Pre-requisites

- NodeJS 8 or later
- NPM

## Build steps

- Setup the environment: `npm install`.
- To build the project: `npm run build`.
- To build the project for a production release: `npm run build-prod`.
- To run the unit tests: `npm run test`.

# Running

## Pre-requisites

To run the end to end tests or the web application the VRUM backend and the VRUM mock server must be up and running,
see: [Running the VRUM backend](chvote-vrum-backend#running) and [Running the VRUM mock server](chvote-vrum-backend#running-the-mock-server).


## E2E tests

To run the e2e tests simply do:

```sh 
npm run e2e
```

## Development mode
 
It is possible to run the web application server in development mode by doing:

```sh
npm run start
```

The web application will be accessible at: [http://localhost:4500](http://localhost:4500).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
