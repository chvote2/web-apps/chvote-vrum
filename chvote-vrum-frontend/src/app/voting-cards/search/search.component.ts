/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatInput } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

import { ConfigurationService, VotintCardIdentifierType } from '../../core/configuration/configuration.service';
import { CountingCircle, VotingCard } from '../../core/voting-cards/voting-card.model';
import { VotingCardsService } from '../../core/voting-cards/voting-cards.service';

@Component({
  selector: 'vrum-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [DatePipe]
})
export class SearchComponent implements OnInit {

  private _votingCards: VotingCard[];
  private _countingCirles: CountingCircle[];

  operationId: number;
  formControl: FormControl;
  identifierTypes: VotintCardIdentifierType[];
  defaultIdentifierType: VotintCardIdentifierType;
  selectedIdentifierType: VotintCardIdentifierType;

  @ViewChild(MatInput)
  input: MatInput;

  constructor(private route: ActivatedRoute,
              private configurationService: ConfigurationService,
              private votingCardsService: VotingCardsService) {
  }

  ngOnInit() {
    this.operationId = Number(this.route.parent.snapshot.params['id']);
    this.formControl = new FormControl('', [Validators.required]);
    this.identifierTypes = this.configurationService.getIdentifierTypes();
    this.defaultIdentifierType = this.configurationService.getDefaultIdentifierType();
    this.selectedIdentifierType = this.defaultIdentifierType;
  }

  get votingCards() {
    return this._votingCards;
  }

  get countingCircles() {
    return this._countingCirles;
  }

  onIdentifierTypeSelected(identifierType: VotintCardIdentifierType): void {
    this.resetView();
    this.selectedIdentifierType = identifierType;
    this.focusOnInput();
  }

  onSubmit() {
    let identifierName = '';
    switch (this.selectedIdentifierType) {
      case VotintCardIdentifierType.VOTER_INDEX:
        identifierName = 'voterIndex';
        break;
      case VotintCardIdentifierType.IDENTIFICATION_CODE:
        identifierName = 'identificationCode';
        break;
      case VotintCardIdentifierType.IDENTIFICATION_CODE_HASH:
        identifierName = 'identificationCodeHash';
        break;
      case VotintCardIdentifierType.LOCAL_PERSON_ID:
        identifierName = 'localPersonId';
    }

    this.votingCardsService.searchByIdentifier(
      this.operationId,
      identifierName,
      this.formControl.value
    )
      .subscribe(
        (votingCards: VotingCard[]) => {
          this.processVotingCards(votingCards);
        }
      );

    this.focusOnInput();
  }

  getVotingCardsByCountingCircle(countingCircle: CountingCircle): VotingCard[] {
    return this._votingCards
      .filter(votingCard => votingCard.countingCircle.businessId === countingCircle.businessId);
  }

  private processVotingCards(votingCards: VotingCard[]) {
    this._votingCards = votingCards;

    if (votingCards.length > 0) {
      this._countingCirles = [];

      votingCards.forEach((votingCard: VotingCard) => {
        const containsCountingCircle = this._countingCirles.find(countingCircle => {
          return countingCircle.businessId === votingCard.countingCircle.businessId;
        }) !== undefined;

        if (!containsCountingCircle) {
          this._countingCirles.push(votingCard.countingCircle);
        }
      });
    }
  }

  private resetView(): void {
    this.formControl.reset();
    this._votingCards = undefined;
    this._countingCirles = undefined;
  }

  private focusOnInput() {
    setTimeout(() => {
      if (this.input) {
        this.input.focus();
      }
    });
  }

}
