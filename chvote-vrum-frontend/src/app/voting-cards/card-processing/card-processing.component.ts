/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { DatePipe } from '@angular/common';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatButtonToggle, MatButtonToggleGroup, MatInput } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { VotingCard, VotingChannel, VotingRightStatus } from '../../core/voting-cards/voting-card.model';
import { VotingCardsService } from '../../core/voting-cards/voting-cards.service';

@Component({
  selector: 'vrum-card-processing',
  templateUrl: './card-processing.component.html',
  styleUrls: ['./card-processing.component.scss'],
  providers: [DatePipe]
})
export class CardProcessingComponent implements OnInit, AfterViewInit {

  private _votingCard: VotingCard;
  private _votingCardStatusMessage: string;
  private _responseStatus: number;

  operationId: number;
  formControl: FormControl;
  votingChannels: string[];
  blockingChannel: string;
  selectedVotingChannel: string;
  searchIcon = 'search';

  @ViewChild(MatInput)
  input: MatInput;

  @ViewChild(MatButtonToggleGroup)
  channelsToggleGroup: MatButtonToggleGroup;

  @ViewChild('blocking')
  blockingToggle: MatButtonToggle;

  constructor(private route: ActivatedRoute,
              private votingCardsService: VotingCardsService,
              private translateService: TranslateService,
              private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.operationId = Number(this.route.parent.snapshot.params['id']);

    this.votingChannels = [
      VotingChannel.MAIL,
      VotingChannel.POLLING_STATION,
      VotingChannel.OTHER
    ];
    this.blockingChannel = 'BLOCKING';
    this.formControl = new FormControl('', [Validators.required]);
  }

  ngAfterViewInit() {
    const toggles = this.channelsToggleGroup._buttonToggles.toArray();
    toggles.push(this.blockingToggle);

    const toggleToFocus = toggles.find((buttonToggle: MatButtonToggle) =>
      buttonToggle ? buttonToggle.value === this.defaultVotingChannel : false
    );

    if (toggleToFocus) {
      toggleToFocus.focus();
    }
  }

  get defaultVotingChannel(): string {
    return this.votingChannels[0];
  }

  get votingCard() {
    return this._votingCard;
  }

  get votingCardStatusMessage() {
    return this._votingCardStatusMessage;
  }

  get responseStatus() {
    return this._responseStatus;
  }

  formatBirthDate(votingCard: VotingCard): string {
    const padStart = (s: string, width: number, fillWith: string) => {
      return s.length < width ? new Array(width - s.length + 1).join(fillWith) + s : s;
    };

    const voterBirthDate = votingCard.voterBirthDate;
    return [
      voterBirthDate.voterBirthDay ? padStart(voterBirthDate.voterBirthDay.toString(), 2, '0') : null,
      voterBirthDate.voterBirthMonth ? padStart(voterBirthDate.voterBirthMonth.toString(), 2, '0') : null,
      voterBirthDate.voterBirthYear.toString()
    ].filter(Boolean).join('/');
  }

  onChannelSelected(votingChannel: string): void {
    this.resetView();
    this.selectedVotingChannel = votingChannel;

    this.focusOnInput();
  }

  onSubmit(): void {
    if (this.formControl.valid) {
      if (this.selectedVotingChannel === this.blockingChannel) {
        this.blockVotingCard();
      } else {
        if (this._votingCard !== undefined
            && this._votingCard.voterIndex === Number(this.formControl.value)
            && this._votingCard.votingRightStatus === VotingRightStatus.AVAILABLE) {
          this.useVotingCard();
        } else {
          this.searchVotingCard()
            .then(() => {
              this.buildVotingCardStatusMessage();
            });
        }
      }

      this.focusOnInput();
    }
  }

  getOnSearchVotingCardStatusClass() {
    const success = 'success';
    const error = 'error';
    const notFound = 'not-found';

    switch (this._votingCard.votingRightStatus) {
      case VotingRightStatus.AVAILABLE:
        return success;
      case VotingRightStatus.USED:
        if (this._responseStatus === 204) {
          return success;
        }

        return error;
      case VotingRightStatus.LOCKED:
        return notFound;
      case VotingRightStatus.BLOCKED:
        return error;
    }
  }

  private searchVotingCard(): Promise<void> {
    return this.votingCardsService.searchByVoterIndex(this.operationId, this.formControl.value)
      .toPromise()
      .then(
        (response: HttpResponse<VotingCard>) => {
          this._responseStatus = response.status;
          this._votingCard = response.body;
        },
        (error: HttpErrorResponse) => {
          this._responseStatus = error.status;
          this._votingCard = undefined;
        }
      )
      .then(() => {
        if (this._votingCard !== undefined &&
            this._votingCard.votingRightStatus === VotingRightStatus.AVAILABLE) {
          this.searchIcon = 'done';
        } else {
          this.searchIcon = 'search';
        }
      });
  }

  private useVotingCard(): void {
    this.votingCardsService.markAsUsed(
      this.operationId,
      Number(this.formControl.value),
      VotingChannel[this.selectedVotingChannel]
    )
      .subscribe(
        (response: HttpResponse<string>) => this.successHandler(response),
        (error: HttpErrorResponse) => this.errorHandler(error)
      );
  }

  private blockVotingCard(): void {
    this.votingCardsService.markAsBlocked(this.operationId, Number(this.formControl.value))
      .subscribe(
        (response: HttpResponse<string>) => this.successHandler(response),
        (error: HttpErrorResponse) => this.errorHandler(error)
      );
  }

  private successHandler(response: HttpResponse<string>) {
    this.searchVotingCard()
      .then(() => {
        this._responseStatus = response.status;
        this.buildVotingCardStatusMessage();
      });
  }

  private errorHandler(error: HttpErrorResponse) {
    this.searchVotingCard()
      .then(() => {
        this._responseStatus = error.status;
        this.buildVotingCardStatusMessage(error.error);
      });
  }

  private buildVotingCardStatusMessage(votingRightStatus?: VotingRightStatus) {
    if (this._responseStatus !== 404) { // no need to build a message that won't be displayed
      let status;
      if (votingRightStatus === undefined && this._votingCard !== undefined) {
        status = this._votingCard.votingRightStatus;
      } else {
        status = JSON.parse(votingRightStatus);
      }

      const translationValues = this.getTranslationValues();
      const messageKey = 'voting-cards.card-processing.search-result.status.'
                         + this._responseStatus + '.' + status;

      this.translateService.get(messageKey, translationValues)
        .subscribe(votingCardStatus => this._votingCardStatusMessage = votingCardStatus);
    }
  }

  private getTranslationValues() {
    let translationValues: { votingDatetime: string, votingChannel: string } = null;
    this.translateService
      .get('voting-cards.voting-channels.' + this._votingCard.votingChannel)
      .subscribe(votingChannel => {
        translationValues = {
          votingDatetime: this.datePipe.transform(this._votingCard.votingDatetime, 'dd/MM/yyyy'),
          votingChannel
        };
      });

    return translationValues;
  }

  private resetView(): void {
    this.formControl.reset();
    this._votingCard = undefined;
    this.searchIcon = 'search';
  }

  private focusOnInput() {
    setTimeout(() => {
      if (this.input) {
        this.input.focus();
      }
    });
  }

}
