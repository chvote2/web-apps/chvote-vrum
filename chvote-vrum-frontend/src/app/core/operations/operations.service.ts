/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { BaseUrlService } from '../base-url/base-url.service';
import { Operation } from './operation.model';

@Injectable({
  providedIn: 'root'
})
export class OperationsService {

  private readonly serviceUrl: string;

  constructor(private http: HttpClient, private baseUrlService: BaseUrlService) {
    this.serviceUrl = this.baseUrlService.backendBaseUrl + '/operations';
  }

  getOperations(): Observable<Operation[]> {
    const params = new HttpParams().set('test', 'false');
    return this.http.get<Operation[]>(`${this.serviceUrl}?${params.toString()}`);
  }

  getOperation(id: number): Observable<Operation> {
    return this.http.get<Operation>(`${this.serviceUrl}/${id}`);
  }

}
