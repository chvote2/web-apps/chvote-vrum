/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

export interface VotingCard {
  id: number;
  operationId: number;
  voterIndex: number;
  identificationCodeHash: string;
  localPersonId: string;
  voterBirthDate: VoterBirthDate;
  votingDatetime?: Date;
  votingRightStatus: VotingRightStatus;
  votingChannel?: VotingChannel;
  countingCircle: CountingCircle;
  domainOfInfluences: DomainOfInfluence[];
}

class VoterBirthDate {
  voterBirthYear: number;
  voterBirthMonth?: number;
  voterBirthDay?: number;
}

export class CountingCircle {
  businessId: string;
  name: string;
}

export class DomainOfInfluence {
  businessId: string;
  name: string;
}

export enum VotingRightStatus {
  AVAILABLE = 'AVAILABLE',
  LOCKED    = 'LOCKED',
  USED      = 'USED',
  BLOCKED   = 'BLOCKED'
}

export enum VotingChannel {
  E_VOTING        = 'E_VOTING',
  MAIL            = 'MAIL',
  POLLING_STATION = 'POLLING_STATION',
  OTHER           = 'OTHER'
}
