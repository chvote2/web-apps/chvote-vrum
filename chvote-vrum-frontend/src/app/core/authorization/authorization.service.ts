/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { BaseUrlService } from '../base-url/base-url.service';
import { User } from './user.model';

@Injectable({providedIn: 'root'})
export class AuthorizationService {

  private _userSnapshot: User;
  private serviceUrl: string;

  public lastVisited: string;
  public userChange = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private baseUrlService: BaseUrlService) {
    this.serviceUrl = this.baseUrlService.backendBaseUrl + '/user';
  }

  updateUser(): Observable<User> {
    if (this.userSnapshot) {
      return of(this.userSnapshot);
    }

    return this.http.get<User>(`${this.serviceUrl}/me`).pipe(
      tap(u => this.userSnapshot = u)
    );
  }

  clear() {
    this.userSnapshot = null;
  }

  hasAtLeastOneRole(roles: string[]) {
    return !!roles.find(role => {
      return this.userSnapshot.roles.includes(role);
    });
  }

  set userSnapshot(value: User) {
    this.userChange.next(value);
    this._userSnapshot = value;
  }

  get userSnapshot(): User {
    return this._userSnapshot;
  }

}
