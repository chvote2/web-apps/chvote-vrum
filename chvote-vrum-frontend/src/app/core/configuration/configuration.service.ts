/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor() {
  }

  getIdentifierTypes(): VotintCardIdentifierType[] {
    return [
      VotintCardIdentifierType.VOTER_INDEX,
      VotintCardIdentifierType.IDENTIFICATION_CODE,
      VotintCardIdentifierType.IDENTIFICATION_CODE_HASH,
      VotintCardIdentifierType.LOCAL_PERSON_ID
    ];
  }

  getDefaultIdentifierType(): VotintCardIdentifierType {
    return VotintCardIdentifierType.LOCAL_PERSON_ID;
  }

}

export enum VotintCardIdentifierType {
  VOTER_INDEX = 'VOTER_INDEX',
  IDENTIFICATION_CODE = 'IDENTIFICATION_CODE',
  IDENTIFICATION_CODE_HASH = 'IDENTIFICATION_CODE_HASH',
  LOCAL_PERSON_ID = 'LOCAL_PERSON_ID'
}
