/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from '../../core/authorization/authorization.service';

import { Operation } from '../../core/operations/operation.model';

@Component({
  selector: 'vrum-operations',
  templateUrl: './operations-list.component.html',
  styleUrls: ['./operations-list.component.scss']
})
export class OperationsListComponent implements OnInit {

  displayedColumns: string[] = ['operationDate', 'shortLabel', 'longLabel', 'arrow'];
  dataSource: MatTableDataSource<Operation>;

  @ViewChild(MatSort)
  sort: MatSort;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    const ops = this.route.snapshot.data.operations;

    this.dataSource = new MatTableDataSource(ops);
    this.dataSource.sort = this.sort;
  }

  onOperationSelected(id: number): void {
    this.router.navigate(['/operations', id, 'card-processing']);
  }

}
