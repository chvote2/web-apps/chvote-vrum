/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-vrum                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';
import { OperationsListController } from './operations-list.po';

export class OperationDetailController {

  private static get operationDetail() {
    return element(by.css('.operation-detail-section'));
  }

  static get isOperationDetailPage() {
    return OperationDetailController.operationDetail.isPresent();
  }

  static get navigationLinks() {
    return element.all(by.css('.mat-tab-link'));
  }

  static get tabCardProcessing() {
    return element(by.css('#mat-tab-card-processing'));
  }

  static get tabSearch() {
    return element(by.css('#mat-tab-search'));
  }

  static get tabRegistryImport() {
    return element(by.css('#mat-tab-registry-import'));
  }

  static open() {
    return OperationsListController.getOperationRow(0).click()
      .then(() => browser.wait(ExpectedConditions.presenceOf(this.operationDetail)));
  }

}
