#---------------------------------------------------------------------------------------------------
# - #%L                                                                                            -
# - chvote-vrum                                                                                    -
# - %%                                                                                             -
# - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
# - %%                                                                                             -
# - This program is free software: you can redistribute it and/or modify                           -
# - it under the terms of the GNU Affero General Public License as published by                    -
# - the Free Software Foundation, either version 3 of the License, or                              -
# - (at your option) any later version.                                                            -
# -                                                                                                -
# - This program is distributed in the hope that it will be useful,                                -
# - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
# - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
# - GNU General Public License for more details.                                                   -
# -                                                                                                -
# - You should have received a copy of the GNU Affero General Public License                       -
# - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
# - #L%                                                                                            -
#---------------------------------------------------------------------------------------------------

apiVersion: v1
kind: Template
labels:
  app: VRUM
  role: reverse-proxy
metadata:
  name: chvote-vrum-reverse-proxy-template
  annotations:
    description: VRUM reverse-proxy deployment template
    iconClass: icon-nodejs
    openshift.io/display-name: VRUM reverse-proxy deployment template
    openshift.io/long-description: VRUM reverse-proxy deployment template
    tags: VRUM
    template.openshift.io/bindable: "false"
  labels:
    app: VRUM
    role: backend
parameters:
- description: domain name to use for the route
  displayName: domain name to use for the route
  name: DOMAIN
  required: true
- displayName: reverse-proxy image name
  name: RP_IMAGE
  required: true
  value: nginx-app-reverse-proxy
- displayName: reverse-proxy image tag
  name: RP_IMAGE_TAG
  required: true
  value: latest
- description: Minimum amount of memory the reverse proxy pod can use.
  displayName: Minimum Memory allocation
  name: RP_MEMORY_REQUEST
  required: true
  value: 100M
- description: Maximum amount of memory the reverse proxy pod can use.
  displayName: Memory Limit
  name: RP_MEMORY_LIMIT
  required: true
  value: 300M
- description: Minimum amount of CPU the reverse proxy pod can use.
  displayName: CPU Request
  name: RP_CPU_REQUEST
  required: true
  value: 100m
- description: Maximum amount of CPU the reverse proxy pod can use.
  displayName: CPU Limit
  name: RP_CPU_LIMIT
  required: true
  value: 100m
objects:
- apiVersion: v1
  kind: Route
  metadata:
    name: vrum-reverse-proxy
    labels:
      app: VRUM
      role: reverse-proxy
  spec:
    host: ${DOMAIN}
    path: /vrum
    port:
      targetPort: http
    tls:
      termination: edge
    to:
      kind: Service
      name: vrum-reverse-proxy
      weight: 100
    wildcardPolicy: None
- apiVersion: v1
  kind: Service
  metadata:
    name: vrum-reverse-proxy
    annotations:
      description: Exposes the VRUM reverse-proxy service
    labels:
      app: VRUM
      role: reverse-proxy
  spec:
    ports:
    - name: http
      port: 8080
      targetPort: 8080
    selector:
      app: VRUM
      role: reverse-proxy
- apiVersion: v1
  kind: ServiceAccount
  metadata:
    name: vrum-reverse-proxy-sa
    labels:
      app: VRUM
      role: reverse-proxy
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${RP_IMAGE}-is
    labels:
      app: VRUM
      role: reverse-proxy
  spec:
    lookupPolicy:
      local: false
    tags:
    - name: ${RP_IMAGE_TAG}
      from:
        kind: ImageStreamTag
        name: ${RP_IMAGE}:${RP_IMAGE_TAG}
        namespace: chvote-images
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: vrum-reverse-proxy-dc
    annotations:
      description: Defines how to deploy the VRUM reverse-proxy
      template.alpha.openshift.io/wait-for-ready: "true"
    labels:
      app: VRUM
      role: reverse-proxy
  spec:
    replicas: 1
    selector:
      name: vrum-reverse-proxy-container
    strategy:
      type: Recreate
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - vrum-reverse-proxy-container
        from:
          kind: ImageStreamTag
          name: ${RP_IMAGE}-is:${RP_IMAGE_TAG}
      type: ImageChange
    - type: ConfigChange
    template:
      metadata:
        labels:
          app: VRUM
          name: vrum-reverse-proxy-container
          role: reverse-proxy
        name: vrum-reverse-proxy-container-template
      spec:
        containers:
        - name: vrum-reverse-proxy-container
          image: ${RP_IMAGE}-is:${RP_IMAGE_TAG}
          ports:
          - containerPort: 8080
          resources:
            limits:
              cpu: ${RP_CPU_LIMIT}
              memory: ${RP_MEMORY_LIMIT}
            requests:
              cpu: ${RP_CPU_REQUEST}
              memory: ${RP_MEMORY_REQUEST}
          volumeMounts:
          - mountPath: /log
            name: logs
          - mountPath: /conf
            name: chvote-vrum-reverse-proxy-config
          livenessProbe:
            exec:
              command:
              - curl
              - http://127.0.0.1:8080/status
            failureThreshold: 3
            initialDelaySeconds: 30
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 10
          readinessProbe:
            exec:
              command:
              - curl
              - http://127.0.0.1:8080/status
            failureThreshold: 3
            initialDelaySeconds: 10
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 10
        serviceAccount: vrum-reverse-proxy-sa
        securityContext:
          runAsGroup: 1234
        volumes:
        - emptyDir: {}
          name: logs
        - configMap:
            defaultMode: "444"
            name: chvote-vrum-reverse-proxy-config
          name: chvote-vrum-reverse-proxy-config