# CHVote: VRUM 
[![pipeline status](https://gitlab.com/chvote2/web-apps/chvote-vrum/badges/master/pipeline.svg)](https://gitlab.com/chvote2/web-apps/chvote-vrum/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-vrum&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-vrum)

The Voting Rights Usage Manager (VRUM) centralizes voting cards and manages voting right usage upon all the available voting channels. It has 2 main roles:

- Managing the voting rights processing.
- Providing and updating voting cards status for the [chvote-receiver](https://gitlab.com/chvote2/web-apps/chvote-receiver) application.

# Components

- [chvote-vrum-backend](chvote-vrum-backend): The server side application component.
- [chvote-vrum-frontend](chvote-vrum-frontend): The web client component.
- [chvote-vrum-reverse-proxy](chvote-vrum-reverse-proxy): The NGINX configuration files of the reverse proxy component.
- [chvote-vrum-docker-compose](chvote-vrum-docker-compose): The set of docker compose description files to create a working environment.
- [chvote-vrum-design-docs](chvote-vrum-design-docs): A module to build the design documentation.
- [deploy](deploy): The Kubernetes deployment configuration files.
- [deploy-openshift](deploy-openshift): The OpenShift deployment configuration files.

# Building

# Build and run

There are 2 components to build and run: the [backend server](chvote-vrum-backend) and the [frontend application](chvote-vrum-frontend).

# Documentation

- [Application design documentation](https://gitlab.com/chvote2/web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-design-docs/target/generated-docs/pdf/vrum-application-design.pdf?job=artifact%3Adesign-docs)
- [REST API contract](https://gitlab.com/chvote2/web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-backend/chvote-vrum-rest/target/site/swagger/swagger.yaml?job=artifact%3Abackend)
- [E2E tests report](https://gitlab.com/chvote2/web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-frontend/test_reports/protractor/index.html?job=check%3Ae2e)
- Backend code coverage reports:
    - [chvote-vrum-service](https://gitlab.com/chvote2/web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-backend/chvote-vrum-service/target/site/jacoco/jacoco.xml?job=artifact%3Abackend)
    - [chvote-vrum-rest](https://gitlab.com/chvote2/web-apps/chvote-vrum/builds/artifacts/master/raw/chvote-vrum-backend/chvote-vrum-rest/target/site/jacoco/jacoco.xml?job=artifact%3Abackend)

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software, released under the [Affero General Public License 3.0](LICENSE) license.
